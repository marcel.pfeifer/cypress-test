import { add } from './add'

test('Add Testing', () => {
    let result = add(2, 2)
    expect(result).toEqual(4)
    result = add(4, 2)
    expect(result).toEqual(6)
    result = add(-2, 2)
    expect(result).toEqual(0)
})