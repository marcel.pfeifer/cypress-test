/// <reference types="cypress" />

describe('Site loads', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000')
    })

    it('Displays header element', () => {
        cy.get('[data-cy=header]')
        .should('be.visible')
    })

    it('Displays Logo element', () => {
        cy.get('[data-cy=logo]')
        .should('be.visible')
    })
})

describe('Links on Homepage are working', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000')
    })

    it('Navigate to Hello Page', () => {
        cy.get('[data-cy=hello-link]')
        .click()
        
        cy.url()
        .should('include', '/hello')
    })

    it('Displays link to React Docs', () => {
        cy.get('[data-cy=link]')
        .should('be.visible')
        .should('have.attr', 'href', 'https://reactjs.org')
    })
})

describe('Links on Hello Page are working', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000/hello')
    })

    it('Navigate to Home Page', () => {
        cy.get('[data-cy=home-link]')
        .click()
        
        cy.url()
        .should('equal', 'http://localhost:3000/')
    })
})